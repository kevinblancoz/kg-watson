////////////////////////////////////
// Helper methods module 
///////////////////////////////////

'use strict';

const os 	= require('os');
let Helper 	= function () {};

Helper.prototype.scoreData = function (score) {
   let scoreColor;
  if (score >= 0.8) {
    scoreColor = '#b9e7c9';
  } else if (score >= 0.6) {
    scoreColor = '#f5d5bb';
  } else {
    scoreColor = '#f4bac0';
  }
  return { score: score, xloc: (score * 312.0), scoreColor: scoreColor };
};



Helper.prototype.deleteUploadedFile = function(readStream) {
  fs.unlink(readStream.path, function (e) {
    if (e) {
      console.log('error deleting %s: %s', readStream.path, e);
    }
  });
};


/**
 * Parse a base 64 image and return the extension and buffer
 * @param  {String} imageString The image data as base65 string
 * @return {Object}             { type: String, data: Buffer }
 */
Helper.prototype.parseBase64Image = function(imageString) {
  var matches = imageString.match(/^data:image\/([A-Za-z-+\/]+);base64,(.+)$/);
  var resource = {};

  if (matches.length !== 3) {
    return null;
  }

  resource.type = matches[1] === 'jpeg' ? 'jpg' : matches[1];
  resource.data = new Buffer(matches[2], 'base64');
  return resource;
};




module.exports = new Helper();
 